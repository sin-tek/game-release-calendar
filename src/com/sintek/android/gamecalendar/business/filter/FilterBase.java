package com.sintek.android.gamecalendar.business.filter;

/**
 * Created by mav5228 on 11/12/14.
 */
public class FilterBase implements FilterItem {

    protected String field = null;
    protected String filter = null;

    protected FilterBase(){
    }

    @Override
    public String getFilter(){
        return filter + ":" + field;
    }

}
