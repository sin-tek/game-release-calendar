package com.sintek.android.gamecalendar.business.filter;

/**
 * Created by mav5228 on 11/12/14.
 */
public class Region extends FilterBase implements FilterItem{
    private String fieldName = "region";

    public Region(String region){
        field = fieldName;
        filter = region;
    }

    static Region create(String regionCode){
       Region region = new Region(regionCode);
       return region;
    }
}
